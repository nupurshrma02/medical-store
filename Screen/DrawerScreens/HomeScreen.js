import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <View style={styles.whitebox}>
        <Text style={{fontWeight:'bold', margin: 10, fontSize:20}}>Order Quickly With Prescription</Text>
        <View style={{ flexDirection: "row" }}>
        <Text style={{margin: 10, marginTop: 2, fontSize:15,}}>upload Prescription and tell us what you need. We do the rest!</Text> 
        <Image source={require('../../Image/doc.jpg')}
        style = {{height: 50, width: 50 }}/>
        </View>
        <View>
          <Text style={{marginLeft:10, marginBottom:3, color:'green'}} >Save 15% on medicines</Text>
        </View>
        <TouchableOpacity onPress={()=> this.props.navigation.navigate('UPStack')} style={styles.Button}>
          <Text style={styles.ButtonText}>Upload</Text>
        </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: `#dcdcdc`,
  },
  whitebox: {
      width: '100%',
      height: 190,
      backgroundColor: '#fff'
  },
  Button: {
    elevation: 20,
    backgroundColor: "#fa8072",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    width:'40%',
    marginLeft: 10,
    marginTop: 10,

  },
  ButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }
})