import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class SettingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Name </Text>
      </View>
      <View style={{}}>
        <TextInput style={styles.Input}/>
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Age </Text>
      </View>
      <View style={{}}>
        <TextInput style={styles.Input}/>
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> Phone Number </Text>
      </View>
      <View style={{}}>
        <TextInput style={styles.Input}/>
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text style={styles.TextStyle}> email </Text>
      </View>
      <View style={{}}>
        <TextInput style={styles.Input}/>
      </View>
      {/* <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddressStack')} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Add Address</Text>
      </TouchableOpacity> */}
      <View style={styles.InputText}>
      <Text style={{fontSize: 15}}>Address</Text>
      <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddressStack')}>
      <Icon name="chevron-right" size={20} color="#000" style={{marginLeft: 300}}/>
      </TouchableOpacity>
      </View>
      <View style={{alignItems: 'center'}}>
      <TouchableOpacity onPress={()=>{}} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Save</Text>
      </TouchableOpacity>
      </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: `#dcdcdc`,
  },
  TextStyle: {
    fontWeight : '100',
    fontSize : 15,
    
  },
  Input : {
    borderColor : '#a9a9a9',
    borderWidth: 1,  
    height: 40,  
    margin: 10,  
    padding: 10, 
    backgroundColor: `#f8f8ff`,
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  InputText:{
    //flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center',
    borderColor : '#a9a9a9',
    borderWidth: 1,  
    height: 40,  
    margin: 10,  
    padding: 10, 
    backgroundColor: `#f8f8ff`,
    marginTop: 35,
  }

})
