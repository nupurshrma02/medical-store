import React, { Component, useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Alert, FlatList, Image, Button } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Appbar, TextInput, RadioButton} from 'react-native-paper';
const AddAddress = ({ navigation }) => {
  
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [Pincode, setPincode] = useState('');
  const [city, setCity] = useState('');
  const [Address, setAddress] = useState('');
  const [Landmark, setLandmark] = useState('');
  const [CNumber, setCNumber] = useState('');
  

return(
  <ScrollView style={styles.contentBody}>
  <TextInput
      label="First Name"
      value={firstname}
      onChangeText={firstname => setFirstname(firstname)}
      style={styles.textInputall}
      />
  <Text/>
  <TextInput
      label="Last Name"
      value={lastname}
      onChangeText={lastname => setLastname(lastname)}
      style={styles.textInputall}
      />
  <Text/>
  <Text/>
  <View>
  </View>
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TextInput
          label="Pincode"
          value={Pincode}
          onChangeText={Pincode => setPincode(Pincode)}
          style={[styles.textInputall, styles.widtthirty]}
          />
      <TextInput
          label="city/state"
          value={city}
          onChangeText={city => setCity(city)}
          style={[styles.textInputall, styles.widtsuxtysix]}
          />
  </View>
  <Text/>
  <Text/>
  <TextInput
      label="Address"
      value={Address}
      onChangeText={Address => setAddress(Address)}
      multiline = {true}
      numberOfLines = {5}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Landmark"
      value={Landmark}
      onChangeText={Landmark => setLandmark(Landmark)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Contact Number"
      value={CNumber}
      onChangeText={CNumber => setCNumber(CNumber)}
      style={styles.textInputall}
      />
<Text/>
  <Button
  onPress={()=>{}}
  title="Add Address"
  color="#4169e1"
  accessibilityLabel="Learn more about this purple button"
  />
  <Text/>
  </ScrollView>
);
}
export default AddAddress;

const styles = StyleSheet.create({
  image: {
      width: '100%'
  },
  contentBody: {
      paddingHorizontal: 15,
      paddingTop:10,
  },
  textInputall: {
      backgroundColor: '#fff'
  },
  widtthirty: {
      width: '30%',
  },
  widtfortyet: {
      width: '48%',
  },
  radios: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
  },
  widtsuxtysix: {
      width: '66%',
  },
});